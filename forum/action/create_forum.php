<?php

if (user_access('forum_for_create')) {
    if (isset($_POST['create'])) {
        $name = mysql_real_escape_string(trim($_POST['name']));
        $description = mysql_real_escape_string(trim($_POST['description']));
        $number = abs(intval($_POST['number']));
        $access = abs(intval($_POST['access']));
        $output = (isset($_POST['output'])) ? 1 : 0;
        $isset_name = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum` WHERE `name` = "'.$name.'"'), 0);
        $isset_number = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum` WHERE `number` = '.$number), 0);
        if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\.\-)]/", $_POST['name'], $m)) {
            ?>
            <div class = 'err'>
                В поле &laquo;название подфорума&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?= $m[0]?></span> символы!.
            </div>
            <?            
        } else
        if (mb_strlen($name) < $set['forum_new_them_name_min_pod'] || mb_strlen($name) > $set['forum_new_them_name_max_pod']) {
            ?>
            <div class = 'err'>
                В поле &laquo;Название подфорума&raquo; можно использовать от <?= $set['forum_new_them_name_min_pod'] ?> до <?= $set['forum_new_them_name_max_pod'] ?> символов.
            </div>
            <?
        } elseif ($number < 0 || $number == NULL || $number == 0) {
            ?>
            <div class = 'err'>Введите уровень.</div>
            <?
        } elseif (mb_strlen($description) > $set['forum_new_them_name_max_pod_opis']) {
            ?>
            <div class = 'err'>Слишком длинное описание подфорума.</div>
            <?
        } elseif ($isset_number > 0) {
            ?>
            <div class = 'err'>Данный уровень уже использует другой подфорум.</div>
            <?
        } elseif ($isset_name > 0) {
            ?>
            <div class = 'err'>Подфорум с таким названием уже существует.</div>
            <?
        } else {
            admin_log('Форум', 'Подфорумы', 'Создание подфорума "'.$name.'".');
            mysql_query('INSERT INTO `forum` SET `name` = "'.$name.'", `description` = "'.$description.'", `number` = '.$number.', `access` = '.$access.', `output` = '.$output);
            $_SESSION['msg'] = '<div class = "msg">Подфорум успешно создан.</div>';
            header('Location: '.FORUM);
            exit;
        }
    } elseif (isset($_POST['cancel'])) {
        header('Location: '.FORUM);
        exit;
    }
    $next_number = mysql_result(mysql_query('SELECT MAX(`number`) FROM `forum`'), 0) + 1;
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a> / Создание подфорума
    </div>
    <form action = '<?= FORUM ?>/?create_forum' method = 'post' class="p_m">
        <strong>Название подфорума (<?= $set['forum_new_them_name_max_pod'] ?> символ(а/ов)):</strong><br />
        <input type = 'text' name = 'name' value = '' /><br /><br />
        <strong>Описание подфорума (<?= $set['forum_new_them_name_max_pod_opis'] ?> символ(а/ов)):</strong><br />
        <textarea name = 'description'></textarea><br /><br />
        <strong>Позиция:</strong> <input type = 'text' name = 'number' value = '<?= $next_number ?>' size = '3' /><br /><br />
        <strong>Доступ:</strong><br />
        <label><input type = 'radio' name = 'access' value = '0' checked = 'checked' /> Все</label><br />
        <label><input type = 'radio' name = 'access' value = '1' /> Только администраторы</label><br />
        <label><input type = 'radio' name = 'access' value = '2' /> Администраторы + модераторы</label><br />
        <br /><strong>Вывод подфорума:</strong><br />
        <label><input type = 'checkbox' name = 'output' value = '1' /> Отображать список разделов вместо описания</label><br /><br />
        <input type = 'submit' name = 'create' value = 'Создать' /> <input type = 'submit' name = 'cancel' value = 'Отменить' />
    </form>
    <?
    include_once '../sys/inc/tfoot.php';
} else {
    header('Location: '.FORUM);
}
exit;

?>